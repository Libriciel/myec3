<?php
/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 09/10/19
 * Time: 10:35
 */

namespace Libriciel\Myec3;

use Libriciel\Myec3\Exception\SocleAlreadyExitsRessourceException;
use Libriciel\Myec3\Exception\SocleException;
use Libriciel\Myec3\Exception\SocleInternalErrorException;
use Libriciel\Myec3\Exception\SocleMissingRessourceException;
use Libriciel\Myec3\Exception\SocleRelationMissingException;
use Libriciel\Myec3\Exception\SocleSyntaxException;
use SimpleXMLElement;

class Socle
{
    const XSD_SOCLE = __DIR__ . "/schema.xsd";
    const XSD_SOCLE_ERROR = __DIR__ . "/error.xsd";

    /**
     * @var UserSocleInterface $User
     */
    protected $Users;

    /**
     * @var StructureSocleInterface $Structures
     */
    protected $Structures;

    /**
     * @var DepartmentSocleInterface $Department
     */
    protected $Department;

    /**
     * Socle constructor.
     * @param UserSocleInterface $Users UsersTable
     * @param StructureSocleInterface $Structures StructuresTable
     * @param DepartmentSocleInterface $Department
     */
    public function __construct(
        UserSocleInterface $Users = null,
        StructureSocleInterface $Structures = null,
        DepartmentSocleInterface $Department = null
    )
    {
        $this->Users = $Users;
        $this->Structures = $Structures;
        $this->Department = $Department;
    }

    /**
     * Add a structure
     * @param string $xmlString xml
     * @return mixed
     * @throws SocleAlreadyExitsRessourceException
     * @throws SocleSyntaxException
     * @throws SocleInternalErrorException
     */
    public function addStructureXml($xmlString)
    {
        $this->validateXml($xmlString);
        /**
         * @var $xml SimpleXMLElement
         */
        $xml = new SimpleXMLElement($xmlString);
        $externalId = strval($xml->externalId);

        //check if $structure already exists
        $structureId = $this->Structures->getId($externalId);
        if (!empty($structureId)) {
            throw new SocleAlreadyExitsRessourceException(SocleException::ORGANISM, $externalId);
        }

        $res = $this->Structures->add($xml);

        if (empty($res)) {
            throw new SocleInternalErrorException(
                SocleException::ORGANISM,
                $externalId,
                SocleException::PUT
            );
        }
        return $res;
    }

    /**
     * update a structure
     * @param string $xmlString xml
     * @param string $externalId externalId
     * @return mixed
     * @throws SocleAlreadyExitsRessourceException
     * @throws SocleInternalErrorException
     * @throws SocleSyntaxException
     */
    public function updateStructureXml($xmlString, $externalId)
    {
        $this->validateXml($xmlString);
        $xml = new SimpleXMLElement($xmlString);

        //check if $structure already exists
        $structureId = $this->Structures->getId($externalId);

        if (empty($structureId)) {
            return $this->addStructureXml($xmlString);
        }

        $res = $this->Structures->update($xml, $structureId);

        if (empty($res)) {
            throw new SocleInternalErrorException(
                SocleException::ORGANISM,
                $externalId,
                SocleException::PUT
            );
        }

        return $res;
    }


    /**
     * delete a structure : On ne fait que la désactiver
     * @param string $externalId externalId
     * @return mixed
     * @throws SocleMissingRessourceException
     * @throws SocleInternalErrorException
     */
    public function deleteStructure($externalId)
    {
        $structureId = $this->Structures->getId($externalId);

        if (empty($structureId)) {
            throw new SocleMissingRessourceException(
                SocleException::ORGANISM,
                $externalId,
                SocleException::DELETE
            );
        }
        $res = $this->Structures->delete($structureId);

        if (empty($res)) {
            throw new SocleInternalErrorException(
                SocleException::ORGANISM,
                $externalId,
                SocleException::DELETE
            );
        }

        return $res;
    }

    /**
     * Add a structure
     * @param string $xmlString xml
     * @return mixed
     * @throws SocleAlreadyExitsRessourceException
     * @throws SocleSyntaxException
     * @throws SocleInternalErrorException
     * @throws SocleRelationMissingException
     */
    public function addDepartmentXml($xmlString)
    {
        $this->validateXml($xmlString);
        /**
         * @var $xml SimpleXMLElement
         */
        $xml = new SimpleXMLElement($xmlString);
        $externalId = strval($xml->externalId);

        //check if $structure already exists
        $departmentId = $this->Department->getId($externalId);
        if (!empty($departmentId)) {
            throw new SocleAlreadyExitsRessourceException(SocleException::DEPARTMENT, $externalId);
        }

        //check if $organism exists
        $structureExternalId = strval($xml->organism->externalId);
        if (empty($this->Structures->getId($structureExternalId))){
            throw new SocleRelationMissingException(
                SocleException::ORGANISM,
                $structureExternalId,
                'POST'
            );
        }

        //check if $parentDepartment exists
        if (strval($xml->rootDepartment) == 'false'){
            $parentDepartmentId = strval($xml->parentDepartment->externalId);
            if (! $this->Department->getId($parentDepartmentId)){
                throw new SocleRelationMissingException(
                    SocleException::DEPARTMENT,
                    $parentDepartmentId,
                    'POST'
                );
            }
        }

        $res = $this->Department->add($xml);

        if (empty($res)) {
            throw new SocleInternalErrorException(
                SocleException::DEPARTMENT,
                $externalId,
                SocleException::PUT
            );
        }
        return $res;
    }

    /**
     * update a structure
     * @param string $xmlString xml
     * @param string $externalId externalId
     * @return mixed
     * @throws SocleAlreadyExitsRessourceException
     * @throws SocleInternalErrorException
     * @throws SocleSyntaxException
     */
    public function updateDepartmentXml($xmlString, $externalId)
    {
        $this->validateXml($xmlString);
        $xml = new SimpleXMLElement($xmlString);

        //check if $department already exists
        $departmentId = $this->Department->getId($externalId);

        if (empty($departmentId)) {
            return $this->addDepartmentXml($xmlString);
        }

        $res = $this->Department->update($xml, $departmentId);

        if (empty($res)) {
            throw new SocleInternalErrorException(
                SocleException::DEPARTMENT,
                $externalId,
                SocleException::PUT
            );
        }

        return $res;
    }

    /**
     * delete a structure : On ne fait que la désactiver
     * @param string $externalId externalId
     * @return mixed
     * @throws SocleMissingRessourceException
     * @throws SocleInternalErrorException
     */
    public function deleteDepartment($externalId)
    {
        $departmentId = $this->Department->getId($externalId);

        if (empty($departmentId)) {
            throw new SocleMissingRessourceException(
                SocleException::DEPARTMENT,
                $externalId,
                SocleException::DELETE
            );
        }
        $res = $this->Department->delete($departmentId);

        if (empty($res)) {
            throw new SocleInternalErrorException(
                SocleException::DEPARTMENT,
                $externalId,
                SocleException::DELETE
            );
        }

        return $res;
    }


    /**
     * Ajoute un utilisateur
     * @param string $xmlString xml
     * @throws SocleAlreadyExitsRessourceException
     * @throws SocleRelationMissingException
     * @throws SocleSyntaxException
     * @throws SocleInternalErrorException
     * @return mixed
     */
    public function addUserXml($xmlString)
    {
        $this->validateXml($xmlString);
        $xml = new SimpleXMLElement($xmlString);
        $externalId = strval($xml->externalId);
        $roleName = strval($xml->roles->role->name);

        $userId = $this->Users->getId($externalId);

        if (!empty($userId)) {
            throw new SocleAlreadyExitsRessourceException(SocleException::AGENT, $externalId);
        }

        if ($this->Department == null) {
            $structureExternalId = strval($xml->organismDepartment->organism->externalId);
            $structureId = $this->Structures->getId($structureExternalId);
            if (empty($structureId)) {
                throw new SocleRelationMissingException(
                    SocleException::ORGANISM,
                    $structureExternalId,
                    SocleException::POST
                );
            }
        } else {
            $departementExternalId = strval($xml->organismDepartment->externalId);
            $structureId = $this->Department->getId($departementExternalId);
            if (empty($structureId)) {
                throw new SocleRelationMissingException(
                    SocleException::DEPARTMENT,
                    $departementExternalId,
                    SocleException::POST
                );
            }
        }


        $roleId = $this->Structures->getRoleId($roleName, $structureId);

        if (empty($roleId)) {
            throw new SocleRelationMissingException(
                SocleException::AGENT,
                $externalId,
                SocleException::POST
            );
        }

        $res = $this->Users->add($xml, $structureId, $roleId);

        if (empty($res)) {
            throw new SocleInternalErrorException(
                SocleException::AGENT,
                $externalId,
                SocleException::POST
            );
        }

        return $res;
    }

    /**
     * update an user
     * @param string $xmlString xml
     * @param string $externalId externalId
     * @throws SocleRelationMissingException
     * @throws SocleSyntaxException
     * @throws SocleInternalErrorException
     * @throws SocleAlreadyExitsRessourceException
     * @return mixed
     */
    public function updateUserXml($xmlString, $externalId)
    {
        $this->validateXml($xmlString);
        $xml = new SimpleXMLElement($xmlString);

        $roleName = strval($xml->roles->role->name);

        $userId = $this->Users->getId($externalId);

        if (empty($userId)) {
            return $this->addUserXml($xmlString);
        }

        $structureExternalId = strval($xml->organismDepartment->organism->externalId);
        $structureId = $this->Structures->getId($structureExternalId);
        $roleId = $this->Structures->getRoleId($roleName, $structureId);

        if (empty($roleId)) {
            throw new SocleRelationMissingException(
                SocleException::AGENT,
                $externalId,
                SocleException::PUT
            );
        }

        $res = $this->Users->update($xml, $userId, $roleId);

        if (empty($res)) {
            throw new SocleInternalErrorException(
                SocleException::AGENT,
                $externalId,
                SocleException::PUT
            );
        }

        return $res;
    }

    /**
     * delete an user : Par choix on ne fait que desactiver l'utilisateur !
     * @param string $externalId externalId
     * @throws SocleMissingRessourceException
     * @throws SocleInternalErrorException
     * @return mixed
     */
    public function deleteUser($externalId)
    {
        $userId = $this->Users->getId($externalId);

        if (empty($userId)) {
            throw new SocleMissingRessourceException(
                SocleException::AGENT,
                $externalId,
                SocleException::DELETE
            );
        }

        $res = $this->Users->delete($userId);

        if (empty($res)) {
            throw new SocleInternalErrorException(
                SocleException::AGENT,
                $externalId,
                SocleException::DELETE
            );
        }

        return $res;
    }

    /**
     * Validate xml with xsd
     * @param string $xml xml
     * @return void
     * @throws SocleSyntaxException
     */
    protected function validateXml($xml)
    {
        try {
            XSDValidator::schemaValidate(self::XSD_SOCLE, $xml);
        } catch (\Exception $e) {
            throw new SocleSyntaxException();
        }
    }
}
