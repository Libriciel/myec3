<?php
/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 10/10/19
 * Time: 11:49
 */

namespace Libriciel\Myec3\Exception;

/**
 * Exception raised when a particular record was not found
 */
class SocleAlreadyExitsRessourceException extends SocleException
{
    /**
     * SocleAlreadyExitsRessourceException.
     * @param string $resource resource
     * @param string $resourceId resourceId
     */
    public function __construct($resource, $resourceId)
    {
        parent::__construct(
            $resource,
            '005',
            'RESOURCE_ALREADY_EXISTS',
            'La resource existe déjà',
            'POST',
            $resourceId,
            409,
            null
        );
    }
}
