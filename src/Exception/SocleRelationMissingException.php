<?php
/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 10/10/19
 * Time: 11:49
 */

namespace Libriciel\Myec3\Exception;

/**
 * Exception raised when a particular record was not found
 */
class SocleRelationMissingException extends SocleException
{
    /**
     * SocleRelationMissingException.
     * @param string $resource resource
     * @param string $resourceId resourceId
     * @param string $methodType method
     */
    public function __construct($resource, $resourceId, $methodType)
    {
        parent::__construct(
            $resource,
            '004',
            'RELATION_MISSING',
            'Une relation avec l\'entité est manquante',
            $methodType,
            $resourceId,
            400,
            null
        );
    }
}
