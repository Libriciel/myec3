# [1.3.0] - 2022-11-02

- Mise à jour du schéma XSD

# 1.2.1

## Evolutions

- Possibilité de créer les users dans un département à la place d'un organisme si le département existe sur le produit

# 1.2.0

- Ajout des endpoint pour les departments

# 1.1.1

- Dernière version pour packagist

# 1.0.1

# Ajout

- Ajout de la CI

# 1.0.0

# Ajout

- Création initiale